package homework4;

import java.util.Random;

public class Human {
    String name;
    String surname;
    int year;
    int iq;
    Pet pet;
    Human mother;
    Human father;
    String[][] schedule;

    void greetPet(){
        if (isPresentPet()) {
            System.out.printf("Привет, %s. \n", this.pet.getNickname());
        }
    }

    void describePet(){
        if (isPresentPet()){
            System.out.printf("У меня есть %s, ему %d лет, он %s. \n", this.pet.getSpecies(), this.pet.getAge(), this.pet.printTrick());
        }
    }

    boolean isTimeToFeed(boolean isTime) {
        if (isPresentPet()){
            int random = getRandomInt(0, 100);
            if (isTime || this.pet.trickLevel > random){
                System.out.println("Хм... покормлю-ка я " + this.pet.nickname + "a.");
                return true;
            }
            System.out.printf("Думаю, %s не голоден. \n",this.pet.nickname);
            return false;
        }
        return false;
    }

    int getRandomInt(int from, int to){
        Random random = new Random();
        return from + random.nextInt(to - from +1);
    }

    boolean isPresentPet() {
        return this.pet != null;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Human {")
                .append("name = " + this.name)
                .append(", surname = " + this.surname)
                .append(", year = " +this.year)
                .append(this.iq != 0 ? ", iq = " + this.iq : "")
                .append(this.mother != null ? ", mother = " + this.mother.name + " " + this.mother.surname :"")
                .append(this.father != null ? ", father = " + this.father.name + " " + this.father.surname :"")
                .append(this.pet != null ? ", pet = " + this.pet.toString() : "")
                .append("}");
        return builder.toString();
        //return String.format("Human {name ='%2s', surname ='%2s', year =%2d, iq =%2d, mother = %2s, father =%2s, pet =%2s}", this.name, this.surname, this.year, this.iq, this.mother, this.father, this.pet.toString());
    }

    Human(){
        name = "Ваня";
        surname = "Попов";
        year = 32;
        iq = 70;
        pet = new Pet();
    }

    Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    Human(String name, String surname, int year, Human mother, Human father){
        this(name, surname, year);
        this.mother = mother;
        this.father = father;
    }

    Human(String name, String surname, int year, Human mother, Human father, int iq, Pet pet, String[][] schedule){
        this(name, surname, year, mother, father);
        this.iq = iq;
        this.pet = pet;
        this.schedule = schedule;
    }
}
