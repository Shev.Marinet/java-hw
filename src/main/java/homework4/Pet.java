package homework4;

import java.util.Arrays;

public class Pet {
    String species;
    String nickname;
    int age;
    int trickLevel;
    String[] habits;

    void eat() {
        System.out.println("Я кушаю!");
    }

    void respond(){
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился! \n" , this.nickname);
    }

    void foul(){
        System.out.println("Нужно хорошо замести следы...");
    }

    public String getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public boolean isTrick (){
        return this.trickLevel > 50;
    }

    public String printTrick() {
        return this.isTrick() ? "очень хитрый" : "почти не хитрый";
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.species + " {")
                .append("nickname = " + this.nickname)
                .append(this.age != 0 ? ", age = " + this.age : "")
                .append(this.trickLevel != 0 ? ", trickLevel = " +this.trickLevel : "")
                .append(this.habits != null ? ", habits = " + Arrays.deepToString(this.habits) : "")
                .append("}");
        return builder.toString();
        //return String.format("%s{nickname ='%2s', age =%2d, trickLevel =%2d, habits =%2s}", this.species, this.nickname, this.age, this.trickLevel, Arrays.deepToString(this.habits));
    }
    Pet(){
        species = "собака";
        nickname = "Шарик";
        age = 5;
        trickLevel = 70;
        habits = new String[]{"спит", "ест", "играет"};
    }

    Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this(species, nickname);
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }
}
