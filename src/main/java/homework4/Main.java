package homework4;

public class Main {
    public static void main(String[] args) {

        String[][] schedule = new String[3][2];
        schedule[0][0] = "вторник";
        schedule[0][1] = "футбол";
        schedule[1][0] = "четверг";
        schedule[1][1] = "бассейн";
        schedule[2][0] = "суббота";
        schedule[2][1] = "робототехника";

        Pet Sharik = new Pet();
        Pet Murka = new Pet("кошка", "Мурка");
        Pet Ryzhyk = new Pet("кот", "Рыжик", 7, 20, new String[]{"прыгает", "играет"});

        Human Yuriy = new Human("Юрий", "Иванов", 35);
        Human Olga = new Human("Ольга", "Иванова", 32);
        Human Yana = new Human("Яна", "Иванова", 9, Olga, Yuriy);

        Human Ivan = new Human();
        Human Ann = new Human("Анна", "Попова", 30);
        Human Andrew = new Human("Андрей", "Попов", 12, Ann, Ivan, 60, Ryzhyk,schedule);

        System.out.println(Sharik);
        System.out.println(Murka);
        System.out.println(Ryzhyk);

        System.out.println(Yuriy);
        System.out.println(Olga);
        System.out.println(Yana);

        System.out.println(Ivan);
        System.out.println(Ann);
        System.out.println(Andrew);

        Yana.greetPet();
        Yana.describePet();

        Andrew.greetPet();
        Andrew.describePet();

        Andrew.pet.eat();
        Andrew.pet.respond();
        Andrew.pet.foul();

        Andrew.isTimeToFeed(true);
        Andrew.isTimeToFeed(false);

    }
}
