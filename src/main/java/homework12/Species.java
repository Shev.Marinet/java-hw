package homework12;

public enum Species {
    FISH,
    DOG,
    DOMESTIC_CAT,
    ROBO_CAT,
    BIRD,
    UNKNOWN;

    public boolean canFly(){
        switch (this){
            case FISH:
            case DOG:
            case DOMESTIC_CAT :
            case ROBO_CAT :
            case UNKNOWN:
                return false;
            default: return true;
        }
    }

    public int numberOfLegs() {
        switch (this){
            case DOG:
            case DOMESTIC_CAT:
            case ROBO_CAT:
                return 4;
            case BIRD:
                return 2;
            default: return 0;
        }
    }

    public boolean hasFur() {
        switch (this){
            case DOG:
            case DOMESTIC_CAT:
                return true;
            default: return false;
        }
    }
}
