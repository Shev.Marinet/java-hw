package homework12.human;

public interface HumanCreator {
    public Human bornChild(String girlName, String boyName);
}
