package homework12.human;

import homework12.pet.Pet;

import java.text.ParseException;
import java.util.Map;

public final class Man extends Human {

    public Man(){
        super();
    }

    public Man(String name, String surname, long birthDate) {
        super(name, surname, birthDate);
    }

    public Man(String name, String surname, long birthDate, int iq) {
        super(name, surname, birthDate, iq);
    }

    public Man(String name, String surname, String birthDate, int iq) throws ParseException {
        super(name, surname, birthDate, iq);
    }

        public Man(String name, String surname, long birthDate, int iq, Map schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    @Override
    public void greetPets() {
        if (isPresentPet()) {
            String petsName = "";
            int counter = 0;
            for (Pet pet : getFamily().getPets()) {
                petsName += pet.getNickname();
                counter++;
                if (counter < getFamily().getPets().size()) petsName += ", ";
            }
            System.out.printf("Привет, %s. Где мои тапки?\n", petsName);
        }
    }

    public void repairCar() {
        System.out.println("Пора сальники поменять");
    }
}
