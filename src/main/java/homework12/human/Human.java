package homework12.human;

import homework12.Family;
import homework12.pet.Pet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.*;

abstract public class Human {

    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Family family;
    private Map schedule;

    Human(){
        name = "Ваня";
        surname = "Попов";
        birthDate = 1349333576;
        iq = 70;
    }

    Human(String name, String surname, long birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }

    Human(String name, String surname, long birthDate, int iq) {
        this(name, surname, birthDate);
        this.iq = iq;
    }

    Human(String name, String surname, String birthDate, int iq) throws ParseException {
        this.name = name;
        this.surname = surname;
        this.iq = iq;
        this.birthDate = convertStringDateToLongDate(birthDate);
    }

    Human(String name, String surname, long birthDate, int iq, Map schedule) {
        this(name, surname, birthDate);
        this.iq = iq;
        this.schedule = schedule;
    }

    public String printMap(Map schedule) {
        String result = "[";
        int counter = 0;
        for (Object name : schedule.keySet()) {
            String key = name.toString();
            String value = schedule.get(name).toString();
            result = result + "[" + key + ", " + value + "]";
            counter++;
            if (counter < schedule.size()) result += ", ";
        }
        return result + "]";
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Human {")
                .append("name = " + this.name)
                .append(", surname = " + this.surname)
                .append(", birth date = " + this.getBirthDateDDMMyyyy())
                .append(this.iq != 0 ? ", iq = " + this.iq : "")
                .append(this.schedule != null ? ", schedule = " + printMap(schedule) : "")
                .append("}");
        return builder.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.printf("Объект %s удален сборщиком мусора.\n", this.toString());
        super.finalize();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Map getSchedule() {
        return schedule;
    }

    public void setSchedule(Map schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public abstract void greetPets();

    public void describePet() {
        if (isPresentPet()) {
            String result = "У меня есть ";
            for (Pet pet : family.getPets()){
                result += pet.getSpecies() + ", ему " + pet.getAge() + " лет, он " + pet.getTrickLevel();
            }
            System.out.println(result);
        }
    }

    public boolean isTimeToFeed(boolean isTime, Pet pet) {
        if (isPresentPet() && family.getPets().contains(pet)) {
            int random = getRandomInt(0, 100);
            if (isTime || pet.getTrickLevel() > random) {
                System.out.println("Хм... покормлю-ка я " + pet.getNickname() + "a.");
                return true;
            }

            System.out.printf("Думаю, %s не голоден. \n", pet.getNickname());
            return false;

        }
        return false;
    }

    int getRandomInt(int from, int to) {
        Random random = new Random();
        return from + random.nextInt(to - from + 1);
    }

    boolean isPresentPet() {
        return family.getPets() != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate &&
                iq == human.iq &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(schedule, human.schedule);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, surname, birthDate, iq);
        result = 31 * result + Objects.hashCode(schedule);
        return result;
    }

    public String getBirthDateDDMMyyyy() {
        Date date = new Date(birthDate*1000L); // convert seconds to milliseconds
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"); // the format of your date
        String formattedDate = dateFormat.format(date);
        return formattedDate;
    }

    public String  describeAge() {
        LocalDate now = LocalDate.now();
        Period period = Period.between(
                new Date(birthDate*1000L).toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                now
        );

        int years =  period.getYears();
        int months =  period.getMonths();
        int days =  period.getDays();
        String result = name + " are " + years + " years " + months + " months " + days + " days old";
        System.out.println(result);
        return result;
    }

    public int getAge() {
        LocalDate now = LocalDate.now();
        Period period = Period.between(
                new Date(birthDate*1000L).toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                now
        );

        return period.getYears();
    }

    public long convertStringDateToLongDate (String dateStr) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date date = format.parse(dateStr);
        return date.getTime() / 1000;
    }

    public String prettyFormat() {
        Formatter formatter = new Formatter();
        formatter.format("%s%s%s%s%s%s%s%s%s%n",
                "name = ", this.name,
                ", surname = ", this.surname,
                ", birth date = ", this.getBirthDateDDMMyyyy(),
                ", iq = ", this.iq,
                this.schedule != null ? ", schedule = " + printMap(schedule) : ", schedule = null");
        return formatter.toString();
    }

    public String prettyFormatChild() {
        Formatter formatter = new Formatter();
        formatter.format("%15s%s%s%s%s%s%s%s%s%s%s%n",
                "",
                this.getClass().getName() == "homework12.human.Woman" ? "girl: " : "boy: ",
                "name = ", this.name,
                ", surname = ", this.surname,
                ", birth date = ", this.getBirthDateDDMMyyyy(),
                ", iq = ", this.iq,
                this.schedule != null ? ", schedule = " + printMap(schedule) : ", schedule = null");
        return formatter.toString();
    }
}