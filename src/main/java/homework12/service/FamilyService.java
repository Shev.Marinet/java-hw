package homework12.service;

import homework12.Family;
import homework12.dao.FamilyDao;
import homework12.human.Human;
import homework12.pet.Pet;

import java.util.*;
import java.util.stream.Collectors;

public class FamilyService {

    private FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies(List<Family> families) {
        families.forEach(family -> System.out.print(family.prettyFormat()));
    }

    public List<Family> getFamiliesBiggerThan(int number) {
        return getAllFamilies()
                .stream()
                .filter(f -> f.countFamily() > number)
                .collect(Collectors.toList());
    }

    public List<Family> getFamiliesLessThan(int number) {
        return getAllFamilies()
                .stream()
                .filter(f -> f.countFamily() < number)
                .collect(Collectors.toList());

    }

    public int countFamiliesWithMemberNumber(int number) {
        return (int) getAllFamilies().stream().filter(f -> f.countFamily() == number).count();
    }

    public Family createNewFamily(Human mother, Human father) {
        Family family = new Family(mother, father);
        familyDao.saveFamily(family);
        return family;
    }

    public Family deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public Human bornChild(Family family, String girlName, String boyName) {
        return family.bornChild(girlName, boyName);
    }

    public boolean adoptChild(Family family, Human child) {
        return family.addChild(child) != null;
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyDao.getAllFamilies()
                .stream()
                .peek(f -> f.getChildren().removeIf(child -> child.getAge() >= age))
                .collect(Collectors.toList());
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return familyDao.getAllFamilies().get(index);
    }

    public Set<Pet> getPets(int index) {
        return getAllFamilies().get(index).getPets();
    }

    public Family addPet(int index, Pet pet) {
        Family family = familyDao.getFamilyByIndex(index);
        if (family.getPets() == null) {
            family.setPets(new HashSet<>(Arrays.asList(pet)));
        } else {
            family.getPets().add(pet);
        }
        return family;
    }

}
