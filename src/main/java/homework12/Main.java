package homework12;

import homework12.board.Board;
import homework12.board.InputOutOfRangeException;
import homework12.controller.FamilyController;
import homework12.dao.CollectionFamilyDao;
import homework12.dao.FamilyDao;
import homework12.service.FamilyService;

import java.text.ParseException;

public class Main {

    public static void main(String[] args) throws ParseException, InputOutOfRangeException {

        FamilyDao fd = new CollectionFamilyDao();
        FamilyService fs = new FamilyService(fd);
        FamilyController fc = new FamilyController(fs);

        Board board = new Board(fc);

        board.executeCommand();
    }
}
