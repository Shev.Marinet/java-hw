package homework12.board;

public class FamilyOverflowException extends RuntimeException {
    public FamilyOverflowException() {
        super("Your family is too large.");
    }
}
