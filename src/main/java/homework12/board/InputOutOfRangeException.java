package homework12.board;

public class InputOutOfRangeException extends Exception{
    public InputOutOfRangeException(String message) {
        super(message);
    }

}
