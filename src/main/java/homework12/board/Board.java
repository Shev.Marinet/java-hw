package homework12.board;

import homework12.Family;
import homework12.controller.FamilyController;
import homework12.human.Human;
import homework12.human.Man;
import homework12.human.Woman;
import homework12.pet.Dog;
import homework12.pet.Pet;
import homework12.pet.RoboCat;

import java.text.ParseException;
import java.util.Scanner;

public class Board {

    private FamilyController fc;

    public Board(FamilyController fc) {
        this.fc = fc;
    }

    String initMenu() throws InputOutOfRangeException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter the number of the menu item:\n" +
                "1. Fill with test data.\n" +
                "2. View entire family list.\n" +
                "3. Display a list of families where the number of people is more than a given. \n" +
                "4. Display a list of families where the number of people is less than a given. \n" +
                "5. Count the number of families where the number of members is equal to a given. \n" +
                "6. Create a new family. \n" +
                "7. Delete family by family index in the general list. \n" +
                "8. Edit family by family index in the general list. \n" +
                "9. Delete all children over a given age. \n" +
                "If you want to complete the work - enter exit. \n");

        while (!sc.hasNextInt()) {
            String command = sc.nextLine();
            if (command.equals("exit")) return command;
            else {
                throw new InputOutOfRangeException("Item does not exist.");
            }
        }
        return sc.next();
    }

    void fillData() throws ParseException {

        String[] girlNames = new String[]{"АННА", "ГАЛЯ", "ЮЛЯ", "АЛЛА", "ЕВГЕНИЯ", "ОЛЬГА", "ЯНА", "АЛЕНА", "ИННА", "МАРИНА"};
        String[] boyNames = new String[]{"ИЛЬЯ", "АНДРЕЙ", "ЮРИЙ", "ОЛЕГ", "ЕВГЕНИЙ", "СЕРГЕЙ", "АЛЕКСАНДР", "АНТОН", "ЯРОСЛАВ", "МИХАИЛ"};

        String girlName = girlNames[Family.getRandomInt(0, 9)];
        String boyName = boyNames[Family.getRandomInt(0, 9)];

        Man yuriy = new Man("Yuriy", "Ivanov", 486213809, 64);
        Woman olga = new Woman("Olga", "Ivanova", 526213809, 75);

        Man ivan = new Man("Ivan", "Popov", 517213809, 90);
        Woman ann = new Woman("Ann", "Popova", 617113809, 78);

        Man oleg = new Man("Oleg", "Kosenko", 527291809, 64);
        Woman inna = new Woman("Inna", "Kosenko", 627217809, 75);

        Man vlad = new Man("Vlad", "Panin", 423151809, 90);
        Woman yana = new Woman("Yana", "Panina", 521197809, 78);

        Pet ryzhyk = new RoboCat("Ryzhyk", 7, 60);
        Pet dollar = new Dog("Dollar", 1, 90);
        Man andrew = new Man("Andrew", "Popov", "04/02/2012", 60);

        fc.createNewFamily(olga, yuriy);
        fc.createNewFamily(ann, ivan);
        fc.createNewFamily(inna, oleg);
        fc.createNewFamily(yana, vlad);

        Family ivanovy = fc.getFamilyById(0);
        Family popovy = fc.getFamilyById(1);
        Family kosenko = fc.getFamilyById(2);
        Family paniny = fc.getFamilyById(3);

        fc.bornChild(ivanovy, girlName, boyName);
        fc.bornChild(ivanovy, girlName, boyName);
        fc.bornChild(ivanovy, girlName, boyName);
        fc.bornChild(popovy, girlName, boyName);
        fc.bornChild(kosenko, girlName, boyName);
        fc.bornChild(paniny, girlName, boyName);

        fc.adoptChild(popovy, andrew);

        fc.addPet(1, ryzhyk);
        fc.addPet(1, dollar);
    }

    void displayAllFamilies() throws InputOutOfRangeException {

        catchEmptyListException();

        fc.displayAllFamilies(fc.getAllFamilies());
    }

    void displayFamiliesBiggerThan() throws InputOutOfRangeException {

        catchEmptyListException();

        Scanner sc = new Scanner(System.in);

        System.out.println("Please enter the age above which you want to display children:");
        validateData(sc);
        int age = sc.nextInt();

        fc.displayAllFamilies(fc.getFamiliesBiggerThan(age));
    }

    void displayFamiliesLessThan() throws InputOutOfRangeException {

        catchEmptyListException();

        Scanner sc = new Scanner(System.in);

        System.out.println("Please enter the age below which you want to display the children:");
        validateData(sc);
        int age = sc.nextInt();

        fc.displayAllFamilies(fc.getFamiliesLessThan(age));
    }

    void countFamiliesWithMemberNumber() throws InputOutOfRangeException {

        catchEmptyListException();

        Scanner sc = new Scanner(System.in);

        System.out.println("Please enter a number of members family:");
        validateData(sc);
        int number = sc.nextInt();

        System.out.printf("Found %s families.%n", fc.countFamiliesWithMemberNumber(number));

    }

    void createFamily() throws ParseException, InputOutOfRangeException {
        Human mother = createHuman("mother", 2);
        Human father = createHuman("father", 1);

        System.out.println("Created family:\n" +
                fc.createNewFamily(mother, father).prettyFormat());
    }

    String inputBirthDate(String human) throws InputOutOfRangeException {

        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the year of birth " + human + ":");
        validateData(sc);
        int birthYear = getValidInt(1900, 2019, sc);

        System.out.println("Enter the month of birth " + human + ":");
        int birthMonth = getValidInt(1, 12, sc);

        System.out.println("Enter the day of birth " + human + ":");
        int birthDay = getValidInt(1, 31, sc);

        return birthDay + "/" + birthMonth + "/" + birthYear;
    }

    Human createHuman(String human, int sexNum) throws ParseException, InputOutOfRangeException {

        Scanner sc = new Scanner(System.in);

        System.out.println("Enter " + human + " name :");
        String name = sc.nextLine();

        System.out.println("Enter " + human + " surname :");
        String surname = sc.nextLine();

        String birthDate = inputBirthDate(human);

        System.out.println("Enter the iq " + human + ":");
        int iq = getValidInt(0, 100, sc);

        if (sexNum == 1) {
            return new Man(name, surname, birthDate, iq);
        } else {
            return new Woman(name, surname, birthDate, iq);
        }
    }

    void deleteFamilyByIndex() throws InputOutOfRangeException {

        catchEmptyListException();

        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the index of the family you want to delete:");
        int index = getValidInt(0, fc.getAllFamilies().size() - 1, sc);

        System.out.println("Deleted family:\n"
                + fc.deleteFamilyByIndex(index).prettyFormat());
    }

    int getNumberMenuForUpdateFamily() throws InputOutOfRangeException {

        Scanner sc = new Scanner(System.in);

        System.out.println("Please enter the number of the menu item:\n" +
                "1. Born child. \n" +
                "2. Adopt child. \n" +
                "3. Return to the main menu.");

        return getValidInt(1, 3, sc);

    }

    Human bornChild() throws InputOutOfRangeException {
        Scanner sc = new Scanner(System.in);

        Family family = findFamily(sc);

        System.out.println("Please enter the girl name:");
        String girlName = sc.nextLine();

        System.out.println("Please enter the boy name:");
        String boyName = sc.nextLine();

        return fc.bornChild(family, girlName, boyName);
    }

    Human adoptChild() throws ParseException, InputOutOfRangeException {

        Scanner sc = new Scanner(System.in);

        Family family = findFamily(sc);

        System.out.println("If it is boy enter 1, if it is girl enter 2:\n");
        int sexNum = getValidInt(1, 2, sc);

        Human child = createHuman("child", sexNum);
        fc.adoptChild(family, child);

        return child;
    }


    Family findFamily(Scanner sc) throws InputOutOfRangeException {
        System.out.println("Please enter the family index number:");
        int id = getValidInt(0, fc.getAllFamilies().size() - 1, sc);
        Family family = fc.getFamilyById(id);

        if (family.countFamily() > 4) {
            throw new FamilyOverflowException();
        }
        return family;
    }

    void updateFamily() throws ParseException, InputOutOfRangeException {
        catchEmptyListException();
        int number = getNumberMenuForUpdateFamily();
        if (number == 1) {
            System.out.printf("Child:\n %6s%s", "", bornChild().prettyFormat());
        }
        if (number == 2) {
            System.out.printf("Child:\n %6s%s", "", adoptChild().prettyFormat());
        }
    }

    void deleteAllChildrenOlderThen() throws InputOutOfRangeException {

        catchEmptyListException();

        Scanner sc = new Scanner(System.in);

        System.out.println("Please enter the age above which you want to remove children:");
        int age = getValidInt(0, 120, sc);
        fc.deleteAllChildrenOlderThen(age);

        System.out.println("Children deleted.");
    }

    int getValidInt(int from, int to, Scanner sc) throws InputOutOfRangeException {
        int num;

        validateData(sc);
        num = Integer.parseInt(sc.nextLine());

        if (num < from || num > to) {
            throw new InputOutOfRangeException("The number must be between " + from + " and " + to);
        }

        return num;
    }

    void validateData(Scanner sc) throws InputOutOfRangeException {
        if (!sc.hasNextInt()) {
            throw new InputOutOfRangeException("It is`n a number");
        }
    }

    void catchEmptyListException() throws InputOutOfRangeException {
        if (fc.getAllFamilies().isEmpty()) {
            throw new InputOutOfRangeException("List of families is empty.");
        }
    }

    public void executeCommand() throws ParseException, InputOutOfRangeException {
        String command;

        while (true) {

            command = initMenu();
            if (command.equals("exit")) {
                break;
            }

            int number = Integer.parseInt(command);

            if (number == 1) {
                fillData();
            }

            if (number == 2) {
                displayAllFamilies();
            }

            if (number == 3) {
                displayFamiliesBiggerThan();
            }

            if (number == 4) {
                displayFamiliesLessThan();
            }

            if (number == 5) {
                countFamiliesWithMemberNumber();
            }

            if (number == 6) {
                createFamily();
            }

            if (number == 7) {
                deleteFamilyByIndex();
            }

            if (number == 8) {
                updateFamily();
            }

            if (number == 9) {
                deleteAllChildrenOlderThen();
            }
        }
    }
}
