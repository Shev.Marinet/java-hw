package homework12.pet;

import homework12.Species;

import java.util.Set;

public class DomesticCat extends Pet implements Pet.Foul {

    {
        DomesticCat.this.setSpecies(Species.DOMESTIC_CAT);
    }

    public DomesticCat(){
        super();
    }

    public DomesticCat(String nickname){
        super(nickname);
    }

    public DomesticCat(String nickname, int age, int trickLevel){
        super(nickname, age, trickLevel);
    }

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits){
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond(){
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился! \n" , this.getNickname());
    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы... и залезть под кровать");
    }

}
