package homework12.pet;

import homework12.Species;

import java.util.Set;

public class Dog extends Pet implements Pet.Foul {

    {
        Dog.this.setSpecies(Species.DOG);
    }

    public Dog(){
        super();
    }

    public Dog(String nickname){
        super(nickname);
    }

    public Dog(String nickname, int age, int trickLevel){
        super(nickname, age, trickLevel);
    }

    public Dog(String nickname, int age, int trickLevel, Set<String> habits){
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond(){
        System.out.printf("Привет, хозяин. Я - %s. Я охранял твой дом! \n" , this.getNickname());
    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы... и спрятаться под столом");
    }

}
