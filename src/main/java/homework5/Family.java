package homework5;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Family {

    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    static {
        System.out.println("Загружается класс Family");
    }

    {
        System.out.println("Создается объект Family");
    }


    Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new Human[]{};
        mother.setFamily(this);
        father.setFamily(this);
    }

    Family(Human mother, Human father, Human[] children) {
        this(mother, father);
        this.children = children;

        mother.setFamily(this);
        father.setFamily(this);

        for (Human child : children) {
            child.setFamily(this);
        }
    }

    Family(Human mother, Human father, Pet pet) {
        this(mother, father);

        this.children = new Human[]{};
        this.pet = pet;

        mother.setFamily(this);
        father.setFamily(this);

    }

    Family(Human mother, Human father, Human[] children, Pet pet) {
        this(mother, father,children);
        this.pet = pet;

        mother.setFamily(this);
        father.setFamily(this);

        for (Human child : children) {
            child.setFamily(this);
        }
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    void greetPet() {
        if (isPresentPet()) {
            System.out.printf("Привет, %s. \n", this.pet.getNickname());
        }
    }

    void describePet() {
        if (isPresentPet()) {
            System.out.printf("У меня есть %s, ему %d лет, он %s. \n", this.pet.getSpecies(), this.pet.getAge(), this.pet.printTrick());
        }
    }

    boolean isTimeToFeed(boolean isTime) {
        if (isPresentPet()) {
            int random = getRandomInt(0, 100);
            if (isTime || this.pet.getTrickLevel() > random) {
                System.out.println("Хм... покормлю-ка я " + this.pet.getNickname() + "a.");
                return true;
            }

            System.out.printf("Думаю, %s не голоден. \n", this.pet.getNickname());
            return false;

        }
        return false;
    }

    int getRandomInt(int from, int to) {
        Random random = new Random();
        return from + random.nextInt(to - from + 1);
    }

    boolean isPresentPet() {
        return this.pet != null;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Family{")
                .append("mother=" + mother)
                .append(", father=" + father)
                .append(", children=" + Arrays.deepToString(children))
                .append(isPresentPet() ? ", pet=" + pet : "");
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father);
        result = 31 * result;
        return result;
    }

    public Human[] overwriteArray(int index, Human[] children){
        for (int i = index; i < children.length - 1; i++){
            children[i] = children[i+1];
        }
        return Arrays.copyOf(this.children, this.children.length - 1);
    }

    public Human[] addChild(Human child) {
        Human[] newArr = Arrays.copyOf(this.children, this.children.length + 1);
        newArr[newArr.length - 1] = child;
        child.setFamily(this);
        setChildren(newArr); ;
        return this.children;
    }

    public boolean deleteChild(int index){
        if (index > 0 && index < this.children.length){
            this.children[index].setFamily(null);
            Human[] newArr = overwriteArray(index, this.children);
            setChildren(newArr);
            return true;
        }

        return false;
    }

    public Human[] deleteChild(Human child){
        for (int i = 0; i < this.children.length; i++) {
            if (child.equals(this.children[i])) {
                this.children[i].setFamily(null);
                Human[] newArr = overwriteArray(i, this.children);
                setChildren(newArr);
            }
        }
        return this.children;
    }

    public int countFamily(){
        return this.children.length + 2;
    }
}
