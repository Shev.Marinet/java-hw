package homework5;

public class Main {

    public static void main(String[] args) {

        String[][] schedule = new String[3][2];
        schedule[0][0] = "вторник";
        schedule[0][1] = "футбол";
        schedule[1][0] = "четверг";
        schedule[1][1] = "бассейн";
        schedule[2][0] = "суббота";
        schedule[2][1] = "робототехника";

        Pet Sharik = new Pet();
        Pet Murka = new Pet("кошка", "Мурка");
        Pet Ryzhyk = new Pet("кот", "Рыжик", 7, 20, new String[]{"прыгает", "играет"});

        Human Yuriy = new Human("Юрий", "Иванов", 35);
        Human Olga = new Human("Ольга", "Иванова", 32);
        Human Yana = new Human("Яна", "Иванова", 9);

        Family Ivanovy = new Family(Olga,Yuriy);
        Ivanovy.addChild(Yana);

        Human Ivan = new Human();
        Human Ann = new Human("Анна", "Попова", 30);
        Human Andrew = new Human("Андрей", "Попов", 12, 60, schedule);
        Family Popovy = new Family(Ann, Ivan, Ryzhyk);

        Popovy.addChild(Andrew);
        Popovy.addChild(Yana);
        Popovy.deleteChild(1);

        System.out.println(Sharik);
        System.out.println(Murka);
        System.out.println(Ryzhyk);

        System.out.println(Yuriy);
        System.out.println(Olga);
        System.out.println(Yana);
        System.out.println(Ivanovy);

        System.out.println(Ivan);
        System.out.println(Ann);
        System.out.println(Andrew);
        System.out.println(Popovy);

        System.out.println(Andrew.getFamily());
        System.out.println(Yana.getFamily());
        System.out.println(Popovy.countFamily());

        Popovy.addChild(Yana);
        Popovy.deleteChild(Andrew);
        System.out.println("Семья Поповых: " + Popovy);
        System.out.println("Семья Андрея: " + Andrew.getFamily());

        Popovy.greetPet();
        Popovy.describePet();

        Popovy.greetPet();
        Popovy.describePet();

        Popovy.getPet().eat();
        Popovy.getPet().respond();
        Popovy.getPet().foul();

        Popovy.isTimeToFeed(true);
        Popovy.isTimeToFeed(false);

    }
}
