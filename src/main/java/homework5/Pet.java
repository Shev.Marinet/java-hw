package homework5;

import java.util.Arrays;
import java.util.Objects;

public class Pet {

    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    static {
        System.out.println("Загружается класс Pet");
    }

    {
        System.out.println("Создается объект Pet");
    }

    Pet(){
        species = "собака";
        nickname = "Шарик";
        age = 5;
        trickLevel = 70;
        habits = new String[]{"спит", "ест", "играет"};
    }

    Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this(species, nickname);

        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    void eat() {
        System.out.println("Я кушаю!");
    }

    void respond(){
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился! \n" , this.nickname);
    }

    void foul(){
        System.out.println("Нужно хорошо замести следы...");
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public String getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public boolean isTrick (){
        return this.trickLevel > 50;
    }

    public String printTrick() {
        return this.isTrick() ? "очень хитрый" : "почти не хитрый";
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.species + " {")
                .append("nickname = " + this.nickname)
                .append(this.age != 0 ? ", age = " + this.age : "")
                .append(this.trickLevel != 0 ? ", trickLevel = " +this.trickLevel : "")
                .append(this.habits != null ? ", habits = " + Arrays.deepToString(this.habits) : "")
                .append("}");

        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;

        return age == pet.age &&
                trickLevel == pet.trickLevel &&
                Objects.equals(species, pet.species) &&
                Objects.equals(nickname, pet.nickname) &&
                Arrays.equals(habits, pet.habits);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(species, nickname, age, trickLevel);
        result = 31 * result + Arrays.hashCode(habits);
        return result;
    }

}
