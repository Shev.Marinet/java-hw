package homework9;

import homework9.controller.FamilyController;
import homework9.dao.CollectionFamilyDao;
import homework9.dao.FamilyDao;
import homework9.human.Man;
import homework9.human.Woman;
import homework9.pet.Pet;
import homework9.pet.RoboCat;
import homework9.service.FamilyService;

public class Main {

    public static void main(String[] args) {

        FamilyDao fd = new CollectionFamilyDao();
        FamilyService fs = new FamilyService(fd);
        FamilyController fc = new FamilyController(fs);
        String[] girlNames = new String[] {"АННА", "ГАЛЯ", "ЮЛЯ", "АЛЛА", "ЕВГЕНИЯ", "ОЛЬГА", "ЯНА", "АЛЕНА", "ИННА","МАРИНА"};
        String[] boyNames = new String[] {"ИЛЬЯ", "АНДРЕЙ", "ЮРИЙ", "ОЛЕГ", "ЕВГЕНИЙ", "СЕРГЕЙ", "АЛЕКСАНДР", "АНТОНЯ", "ЯРОСЛАВ","МИХАИЛ"};

        String girlName = girlNames[Family.getRandomInt(0,9)];
        String boyName = boyNames[Family.getRandomInt(0,9)];

        Man yuriy = new Man("Yuriy", "Ivanov", 35,64);
        Woman olga = new Woman("Olga", "Ivanova", 32,75);

        Man ivan = new Man("Ivan", "Popov", 33,90);
        Woman ann = new Woman("Ann", "Popova", 30,78);

        Man oleg = new Man("Oleg", "Kosenko", 35,64);
        Woman inna = new Woman("Inna", "Kosenko", 32,75);

        Man vlad = new Man("Vlad", "Panin", 33,90);
        Woman yana = new Woman("Yana", "Panina", 30,78);

        Pet ryzhyk = new RoboCat("Рыжик", 7, 60);
        Man andrew = new Man("Andrew", "Popov", 12);


        fc.createNewFamily(olga,yuriy);
        fc.createNewFamily(ann,ivan);
        fc.createNewFamily(inna, oleg);
        fc.createNewFamily(yana, vlad);

        Family ivanovy = fc.getFamilyById(0);
        Family popovy = fc.getFamilyById(1);
        Family kosenko = fc.getFamilyById(2);
        Family paniny = fc.getFamilyById(3);

        fc.bornChild(ivanovy, girlName, boyName);
        fc.bornChild(ivanovy, girlName, boyName);
        fc.bornChild(ivanovy, girlName, boyName);
        fc.bornChild(popovy, girlName, boyName);
        fc.bornChild(kosenko, girlName, boyName);
        fc.bornChild(paniny, girlName, boyName);

        fc.adoptChild(popovy, andrew);

        System.out.println(fc.countFamiliesWithMemberNumber(4));
        System.out.println(fc.count());

        fc.getAllFamilies();
        fc.displayAllFamilies();

        System.out.println(fc.getFamiliesBiggerThan(4));
        System.out.println(fc.getFamiliesLessThan(4));

        fc.displayAllFamilies();
        fc.addPet(1, ryzhyk);

        System.out.println(fc.getPets(1));

        fc.deleteAllChildrenOlderThen(5);
        fc.displayAllFamilies();
        fc.deleteFamilyByIndex(0);
        fc.displayAllFamilies();

    }
}
