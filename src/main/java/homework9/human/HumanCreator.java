package homework9.human;

public interface HumanCreator {
    public Human bornChild(String girlName, String boyName);
}
