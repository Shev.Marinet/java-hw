package homework9.dao;

import homework9.Family;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao{

    private static final List<Family> families = new ArrayList<>();

    public List<Family> getAllFamilies() {
        return new ArrayList<>(families);
    }

    public Family getFamilyByIndex(int index) {
        return getAllFamilies().get(index);
    };

    public Family deleteFamily(int index) {
        if (index >= 0 && index < families.size()){
            return families.remove(index);
        }
        return null;
    }

    public boolean deleteFamily(Family family) {
        return families.remove(family);

    }

    public List<Family> saveFamily(Family family) {
        if (families.contains(family)){
            families.set(families.indexOf(family), family);
        } else {
            families.add(family);
        }
        return new ArrayList<>(families);
    }

}
