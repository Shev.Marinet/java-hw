package homework9.service;

import homework9.human.Human;
import homework9.Family;
import homework9.dao.FamilyDao;
import homework9.pet.Pet;

import java.util.*;

public class FamilyService {

    private FamilyDao familyDao;

    public FamilyService (FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies(){
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        System.out.println(familyDao.getAllFamilies());
    }

    public List<Family> getFamiliesBiggerThan(int number) {
        List<Family> familiesBiggerThan = new ArrayList<>();
        for (Family family : getAllFamilies()) {
            if (family.countFamily() > number) {
                familiesBiggerThan.add(family);
            }
        }
        return familiesBiggerThan;
    }

    public List<Family> getFamiliesLessThan(int number) {
        List<Family> familiesLessThan = new ArrayList<>();
        for (Family family : getAllFamilies()) {
            if (family.countFamily() < number) {
                familiesLessThan.add(family);
            }
        }
        return familiesLessThan;
    }

    public int countFamiliesWithMemberNumber(int number) {
        int counter = 0;
        for (Family family : getAllFamilies()) {
            if (family.countFamily() == number) {
                counter++;
            }
        }
        return counter;
    }

    public Family createNewFamily(Human mother, Human father) {
        Family family = new Family(mother,father);
        familyDao.saveFamily(family);
        return family;
    }

    public Family deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public Human bornChild(Family family, String girlName, String boyName) {
        return family.bornChild(girlName, boyName);
    }

    public boolean adoptChild(Family family, Human child) {
        return family.addChild(child) != null;
    }

    public boolean deleteAllChildrenOlderThen (int age) {
        int counter = 0;
        for (Family family : familyDao.getAllFamilies()) {
            Iterator<Human> iter = family.getChildren().iterator();
            while (iter.hasNext()) {
                Human child = iter.next();
                if (child.getYear() > age) {
                    iter.remove();
                    counter++;
                }
            }
        }
      return counter != 0;
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return familyDao.getAllFamilies().get(index);
    }

    public Set<Pet> getPets(int index) {
        return getAllFamilies().get(index).getPets();
    }

    public Family addPet(int index, Pet pet) {
        Family family = familyDao.getFamilyByIndex(index);
        family.setPets(new HashSet<>(Arrays.asList(pet)));
        return family;
    }

}
