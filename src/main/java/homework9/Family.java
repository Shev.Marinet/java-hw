package homework9;

import homework9.human.Human;
import homework9.human.HumanCreator;
import homework9.human.Man;
import homework9.human.Woman;
import homework9.pet.Pet;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class Family implements HumanCreator {

    private Human mother;
    private Human father;
    private List<Human> children;
    private Set<Pet> pets;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new ArrayList<>();
        mother.setFamily(this);
        father.setFamily(this);
    }

    Family(Human mother, Human father, List<Human> children) {
        this(mother, father);
        this.children = children;

        mother.setFamily(this);
        father.setFamily(this);

        for (Human child : children) {
            child.setFamily(this);
        }
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPets() {
        return pets;
    }

    public void setPets(Set<Pet> pets) {
        this.pets = pets;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Family{")
                .append("mother=" + mother)
                .append(", father=" + father)
                .append(", children=" + Arrays.toString(children.toArray()))
                .append("}");
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father);
        result = 31 * result;
        return result;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.printf("Объект %s удален сборщиком мусора.\n", this.toString());
        super.finalize();

    }

    public List<Human> addChild(Human child) {
        this.children.add(child);
        child.setFamily(this);
        return getChildren();
    }

    public boolean deleteChild(int index){
        if (index > 0 && index < this.children.size()){
            this.children.remove(index).setFamily(null);
            return true;
        }
        return false;
    }

    public boolean deleteChild(Human child){
        if (getChildren().contains(child)){
            this.children.remove(child);
            child.setFamily(null);
            return true;
        }
        return false;
    }

    public int countFamily(){
        return this.children.size() + 2;
    }

    public static int getRandomInt(int from, int to){
        Random random = new Random();
        return from + random.nextInt(to - from + 1);
    }

    @Override
    public Human bornChild(String girlName, String boyName){
        int sex = getRandomInt(0,1);
        Human child = (sex == 0) ? new Man() : new Woman();
        child.setFamily(this);
        addChild(child);
        child.setSurname(this.father.getSurname());
        child.setName(sex == 0 ? boyName : girlName);
        child.setIq(Math.round((mother.getIq() + father.getIq())/2));
        child.setYear(0);
        return child;
    }
}
