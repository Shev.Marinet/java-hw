package homework3;

import java.util.Arrays;

public class Schedule {
    private final int dimX;
    private final int dimY;
    private String[][] data;

    public Schedule(int dimX, int dimY) {
        this.dimX = dimX;
        this.dimY = dimY;
        this.data = new String[dimX][dimY];
    }
    public static void initialData(String[][] data){
        data[0][0] = "Sunday";;
        data[0][1] = "do home work";
        data[1][0] = "Monday";
        data[1][1] = "go to courses; watch a film";
        data[2][0] = "Tuesday";
        data[2][1] = "go to doctor";
        data[3][0] = "Wednesday";
        data[3][1] = "shopping";
        data[4][0] = "Thursday";
        data[4][1] = "go to courses; clean in the house";
        data[5][0] = "Friday";
        data[5][1] = "go to the birthday";
        data[6][0] = "Saturday";
        data[6][1] = "have a rest";
    }

    public String[][] getData() {
        return data;
    }

    public static void setData(String[][] data, int x, int y, String val) {
        data[x][y] = val;
    }

    public String getTask(String str) {
        for (int i = 0; i < data.length; i++) {
            if (data[i][0].toLowerCase().equals(str.toLowerCase())) return data[i][1];
        }
        return "noTask";
    }
    public String getDay(String str) {
        for (int i = 0; i < data.length; i++) {
            if (data[i][0].toLowerCase().equals(str.toLowerCase())) return data[i][0];
        }
        return "noDay";
    }

    public  void setTask(String day, String task) {
        for (int i = 0; i < data.length; i++) {
            if (data[i][0].toLowerCase().equals(day.toLowerCase())) {
                data[i][1] = task;
            }
        }
    }

    public static void main(String[] args) {
        Schedule schedule = new Schedule(7,2);
        System.out.println(Arrays.deepToString(schedule.getData()));
        String[][] data = schedule.data;
        initialData(data);
        System.out.println(Arrays.deepToString(schedule.getData()));
        setData(data, 6, 0, "YOOO");
        System.out.println(Arrays.deepToString(schedule.getData()));

    }
}
