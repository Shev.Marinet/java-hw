package homework3;

import java.util.Arrays;
import java.util.Scanner;

public class TaskPlanner {

    public static String askTask(Schedule schedule, String day){
        Scanner in = new Scanner(System.in);
        System.out.println("Please, input new tasks for " + schedule.getDay(day));
        return in.nextLine();
    }
    public static boolean isPresentTask(String task){
        if(task == "noTask") {
            System.out.println("Sorry, I don't understand you, please try again.");
            return false;
        }
        return true;
    }

    public static void inputText(Schedule schedule){
        Scanner in = new Scanner(System.in);
        while (true) {
            System.out.println("Please, input the day of the week: ");
            String[] words = in.nextLine().split(" ");
            switch (words.length){
                case 1:
                    if (words[0].equals("exit")) break;
                    if (!isPresentTask(schedule.getTask(words[0]))){
                        continue;
                    } else {
                        System.out.printf("Your tasks for %s is: %s \n", schedule.getDay(words[0]), schedule.getTask(words[0]));
                    }
                    break;
                case 2 :
                    if (!isPresentTask(schedule.getDay(words[1]))){
                        continue;
                    }
                    if (words[0].equals("change") || words[0].equals("reschedule")) {
                        schedule.setTask(words[1],askTask(schedule, words[1]));
                        continue;
                    } else {
                        System.out.println("Sorry, I don't understand you, please try again.");
                    }
                    break;
                default :
                    System.out.println("Sorry, I don't understand you, please try again.");
            }
        }
    }


    public static void main(String[] args) {
        Schedule schedule = new Schedule(7,2);
        String[][] data = schedule.getData();
        schedule.initialData(data);
        System.out.println(Arrays.deepToString(schedule.getData()));
        inputText(schedule);
    }
}
