package homework2;

public class Point {
    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public int x() {
        return x;
    }

    public int y() {
        return y;
    }

    public static void main(String[] args) {
        Point p = new Point(3, 5);
        System.out.println(p);
        Point p3 = new Point(1, 1);
        System.out.println(p3);
    }
}
