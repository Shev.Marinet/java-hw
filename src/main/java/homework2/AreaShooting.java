package homework2;

import java.util.Random;
import java.util.Scanner;

public class AreaShooting {

    public static int randomNumber(int from, int to){
        Random random = new Random();
        return from + random.nextInt(to - from + 1);
    }
    public static int[][] randomArray(){
        int x = randomNumber(2,4);
        int y = randomNumber(1,5);
        int[][] randomArr = new int[3][1];
        randomArr[0] = new int[]{x-1,y};
        randomArr[1] = new int[]{x,y};
        randomArr[2] = new int[]{x+1,y};
//        System.out.println(Arrays.deepToString(randomArr));
        return randomArr;
    }
    public static int[][] reverseArray(int[][] arr){
        for (int i = 0; i < arr.length; i++){
            int num = arr[i][0];
            arr[i][0] = arr[i][1];
            arr[i][1] = num;
        }
//        System.out.println("reverse"+Arrays.deepToString(arr));
        return arr;
    }

    public static int[][] randomTarget(){
        int position = randomNumber(0,1);
        // position 0 - horizontal, position 1 - vertical
        if (position == 0){
            return randomArray();
        }
        return reverseArray(randomArray());
    }

    public static int inputPoint(String axis) {
        Scanner in = new Scanner(System.in);
        int num;
        do {
            System.out.print("Please enter " + axis + " number:  ");
            while (!in.hasNextInt()) {
                System.out.print("It isn't a number. Please enter correct " + axis + " number:  ");
                in.next();
            }
            num = in.nextInt();
        } while (!isOnBoard(num));
        return num;
    }
    public static boolean isOnBoard(int num) {
        return num >= 1 && num <= 5;
    }
    public static void shoot(Board board, int[][] target){
        for (int[] item : target ) {
            Point point = new Point(item[0],item[1]);
            board.setTarget(point);
        }
        int counter = target.length;
        while (counter != 0){
            int x = inputPoint("column");
            int y = inputPoint("row");
            Point shoot = new Point(x,y);
            if (board.get(shoot) == 0) {
                board.setMissedTarget(shoot);
                System.out.println(":( Try again!");
            }
            if (board.get(shoot) == 10) {
                board.setHitedTarget(shoot);
                System.out.println("You hit the target!");
                counter--;
            }
            board.printMe();
        }
        System.out.println("You won!");
    }
    public static void main(String[] args) {
        System.out.println("All set. Get ready to rumble!");
        Board board = new Board(6,6);
        board.printMe();
        int[][] target = randomTarget();
        shoot(board, target);
    }
}
