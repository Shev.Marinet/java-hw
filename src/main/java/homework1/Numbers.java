package homework1;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Numbers {
    public static int randomNumber(int from, int to){
        Random random = new Random();
        return from + random.nextInt(to - from + 1);
    }

    public static int randomData (String [][] matrix){
        return randomNumber(0, matrix.length - 1);
    }

    public static int inputNumber(String question) {
        Scanner in = new Scanner(System.in);
        System.out.print("Please, enter the year of " + question + "  ");
        while (!in.hasNextInt()) {
            System.out.print("Please, enter the year of " + question + "  ");
            in.next();
        }
        return in.nextInt();
    }

    public static String inputName() {
        Scanner in = new Scanner(System.in);
        System.out.print("Input your name: ");
        return in.nextLine();
    }

    public static int[] addNumber(int[] arr, int num){
        int[] newArr = Arrays.copyOf(arr, arr.length + 1);
        newArr[newArr.length - 1] = num;
        return newArr;
    }
    public static int[] compareNumbers(int randomNumber, int num, int[] arr, String question) {
        while (num != randomNumber) {
            System.out.printf("Your number: %d \n", num);
            if (num < randomNumber) {
                System.out.println("Your number is too small. Please, try again.");
            }
            if (num > randomNumber) {
                System.out.println("Your number is too big. Please, try again.");
            }
            num = inputNumber(question);
            arr = addNumber(arr, num);
        }
        return arr;
    }

    public static int[] sortNumbers (int[] arr) {
        for (int item : arr) {
            for (int i = 0; i < arr.length - 1; i++)  {
                if (arr[i] > arr[i+1]){
                    int n = arr[i+1];
                    arr[i+1] = arr[i];
                    arr[i] = n;
                }
            }
        }
        return arr;
    }

    public static void printNumbers(int[] arr){
        System.out.print("Your numbers: ");
        for (int item : arr){
            System.out.print(" " + item);
        }
    }

    public static void main(String[] args) {
        System.out.println("Let the game begin!");
        Matrix matrix = new Matrix(15,2);
        String[][] data = matrix.getMatrix();
        matrix.initiatedMatrix(data);
//        System.out.println(Arrays.deepToString(data));
        int randomNumber = randomData(data);
        int randomYear = Integer.parseInt(data[randomNumber][0]);
        String name = inputName();
        int num = inputNumber(data[randomNumber][1]);
        int [] numbers = {num};
        numbers = compareNumbers(randomYear, num, numbers, data[randomNumber][1]);
        int[] sortNumbers = sortNumbers(numbers);
        System.out.printf("Congratulations, %s !!! \n", name);
        printNumbers(sortNumbers);
    }
}
