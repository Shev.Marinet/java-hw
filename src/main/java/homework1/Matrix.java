package homework1;

public class Matrix {
        private final int dimX;
        private final int dimY;
        private String[][] data;

        public Matrix(int dimX, int dimY){
                this.dimX = dimX;
                this.dimY = dimY;
                this.data = new String[dimX][dimY];
        }

        public final void initiatedMatrix(String [][] data){
                data[0][0] = "476";
                data[0][1] = "the fall of the Western Roman Empire";
                data[1][0] = "610";
                data[1][1] = "the rise of Islam";
                data[2][0] = "962";
                data[2][1] = "formation of the Holy Roman Empire";
                data[3][0] = "1337";
                data[3][1] = "the beginning of the Hundred Years War";
                data[4][0] = "1440";
                data[4][1] = "invention of typography by I. Guttenberg";
                data[5][0] = "1455";
                data[5][1] = "the beginning of the Scarlet and White Rose war in England";
                data[6][0] = "1453";
                data[6][1] = "the fall of the Byzantine Empire";
                data[7][0] = "1492";
                data[7][1] = "discovery of America by Christopher Columbus";
                data[8][0] = "1569";
                data[8][1] = "polish-Lithuanian Commonwealth formation";
                data[9][0] = "1572";
                data[9][1] = "bartholomew's night in France";
                data[10][0] = "1642";
                data[10][1] = "start of civil war in England";
                data[11][0] = "1789";
                data[11][1] = "adoption of the Declaration of Human Rights and Citizens";
                data[12][0] = "1804";
                data[12][1] = "proclamation of Napoleon as emperor of France";
                data[13][0] = "1914";
                data[13][1] = "the beginning of the First world war";
                data[14][0] = "1939";
                data[14][1] = "the beginning of the Second world war";
        }

        public String[][] getMatrix(){
                return data;
        }
}
