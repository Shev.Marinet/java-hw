package homework6;

public enum Species {
    КОШКА,
    СОБАКА,
    ПОПУГАЙ,
    ХОМЯК;

    public boolean canFly(){
        switch (this){
            case КОШКА:
            case СОБАКА:
            case ХОМЯК :
                return false;
            default: return true;
        }
    }

    int numberOfLegs() {
        switch (this){
            case КОШКА:
            case СОБАКА:
            case ХОМЯК :
                return 4;
            default: return 2;
        }
    }

    boolean hasFur() {
        switch (this){
            case КОШКА:
            case СОБАКА:
            case ХОМЯК :
                return true;
            default: return false;
        }
    }
}
