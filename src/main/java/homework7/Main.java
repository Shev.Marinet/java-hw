package homework7;

import homework7.human.Human;
import homework7.human.Man;
import homework7.human.Woman;
import homework7.pet.Dog;
import homework7.pet.DomesticCat;
import homework7.pet.RoboCat;
import homework7.pet.Pet;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        String[][] schedule = new String[3][2];
        schedule[0][0] = DayOfWeek.ВТОРНИК.name();
        schedule[0][1] = "футбол";
        schedule[1][0] = DayOfWeek.ЧЕТВЕРГ.name();
        schedule[1][1] = "бассейн";
        schedule[2][0] = DayOfWeek.СУББОТА.name();
        schedule[2][1] = "робототехника";

        Dog sharik = new Dog();
        DomesticCat murka = new DomesticCat("Мурка");
        Pet ryzhyk = new RoboCat("Рыжик", 7, 20, new String[]{"прыгает", "играет"});

        Man yuriy = new Man("Юрий", "Иванов", 35,64);
        Woman olga = new Woman("Ольга", "Иванова", 32,75);
        Human yana = new Woman("Яна", "Иванова", 9,murka);

        Family ivanovy = new Family(olga,yuriy);
        ivanovy.addChild(yana);

        Human ivan = new Man();
        Human ann = new Woman("Анна", "Попова", 30);
        Human andrew = new Man("Андрей", "Попов", 12, 60, schedule,ryzhyk);
        Family popovy = new Family(ann, ivan);

        popovy.addChild(andrew);
        popovy.addChild(yana);
        popovy.deleteChild(1);

        System.out.println(sharik);
        System.out.println(murka.getSpecies().canFly());
        System.out.println(murka.getSpecies().hasFur());
        System.out.println(murka.getSpecies().numberOfLegs());
        System.out.println(ryzhyk);

        System.out.println(yuriy);
        System.out.println(olga);
        System.out.println(yana);
        System.out.println(ivanovy);

        System.out.println(ivan);
        System.out.println(ann);
        System.out.println(andrew);
        System.out.println(popovy);

        System.out.println(andrew.getFamily());
        System.out.println(yana.getFamily());
        System.out.println(popovy.countFamily());

        popovy.addChild(yana);
        System.out.println(Arrays.toString(popovy.deleteChild(andrew)));
        System.out.println(popovy);
        System.out.println(andrew.getFamily());

        yana.greetPet();
        yana.describePet();

        andrew.greetPet();
        andrew.describePet();

        yana.getPet().eat();
        yana.getPet().respond();
        murka.foul();

        yana.isTimeToFeed(true);
        yana.isTimeToFeed(false);

        yuriy.repairCar();
        olga.makeup();

        System.out.println(ivanovy.bornChild());
    }
}
