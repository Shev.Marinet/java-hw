package homework7.human;

import homework7.Family;
import homework7.pet.Pet;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

abstract public class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private Family family;
    private String[][] schedule;

    static {
        System.out.println("Загружается класс Human");
    }

    {
        System.out.println("Создается объект Human");
    }

    Human(){
        name = "Ваня";
        surname = "Попов";
        year = 32;
        iq = 70;
    }

    Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    Human(String name, String surname, int year, Pet pet) {
        this(name, surname, year);
        this.pet = pet;
    }

    Human(String name, String surname, int year, int iq) {
        this(name, surname, year);
        this.iq = iq;
    }

    Human(String name, String surname, int year, int iq, String[][] schedule) {
        this(name, surname, year);
        this.iq = iq;
        this.schedule = schedule;
    }

    Human(String name, String surname, int year, int iq, String[][] schedule, Pet pet) {
        this(name, surname, year, iq, schedule);
        this.pet = pet;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Human {")
                .append("name = " + this.name)
                .append(", surname = " + this.surname)
                .append(", year = " +this.year)
                .append(this.iq != 0 ? ", iq = " + this.iq : "")
                .append(this.schedule != null ? ", schedule = " + Arrays.deepToString(this.schedule) : "")
                .append("}");
        return builder.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.printf("Объект %s удален сборщиком мусора.\n", this.toString());
        super.finalize();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }


    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public abstract void greetPet();

    public void describePet() {
        if (isPresentPet()) {
            System.out.printf("У меня есть %s, ему %d лет, он %s. \n", this.pet.getSpecies(), this.pet.getAge(), this.pet.printTrick());
        }
    }

    public boolean isTimeToFeed(boolean isTime) {
        if (isPresentPet()) {
            int random = getRandomInt(0, 100);
            if (isTime || this.pet.getTrickLevel() > random) {
                System.out.println("Хм... покормлю-ка я " + this.pet.getNickname() + "a.");
                return true;
            }

            System.out.printf("Думаю, %s не голоден. \n", this.pet.getNickname());
            return false;

        }
        return false;
    }

    int getRandomInt(int from, int to) {
        Random random = new Random();
        return from + random.nextInt(to - from + 1);
    }

    boolean isPresentPet() {
        return this.pet != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year &&
                iq == human.iq &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Arrays.equals(schedule, human.schedule);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, surname, year, iq);
        result = 31 * result + Arrays.hashCode(schedule);
        return result;
    }

}
