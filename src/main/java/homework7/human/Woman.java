package homework7.human;

import homework7.pet.Pet;

public final class Woman extends Human {

    public Woman(){
        this.setName("Инна");
        this.setSurname("Попова");
        this.setYear(32);
        this.setIq(70);
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, Pet pet) {
        super(name, surname, year, pet);
    }

    public Woman(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
    }

    public Woman(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Woman(String name, String surname, int year, int iq, String[][] schedule, Pet pet) {
        super(name, surname, year, iq, schedule, pet);
    }

    @Override
    public void greetPet() {
        if (isPresentPet()) {
            System.out.printf("Привет, %s. Я по тебе соскучилась!\n", this.getPet().getNickname());
        }
    }

    public void makeup() {
        System.out.println("Пора сделать вечерий макияж");
    }
}
