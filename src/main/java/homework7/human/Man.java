package homework7.human;

import homework7.pet.Pet;

public final class Man extends Human{

    public Man(){
        super();
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, Pet pet) {
        super(name, surname, year, pet);
    }

    public Man(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
    }

    public Man(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Man(String name, String surname, int year, int iq, String[][] schedule, Pet pet) {
        super(name, surname, year, iq, schedule,pet);
    }

    @Override
    public void greetPet() {
        if (isPresentPet()) {
            System.out.printf("Привет, %s. Где мои тапки?\n", this.getPet().getNickname());
        }
    }

    public void repairCar() {
        System.out.println("Пора сальники поменять");
    }
}
