package homework7;

import homework7.human.Human;
import homework7.human.HumanCreator;
import homework7.human.Man;
import homework7.human.Woman;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Family implements HumanCreator {

    private Human mother;
    private Human father;
    private Human[] children;

    static {
        System.out.println("Загружается класс Family");
    }

    {
        System.out.println("Создается объект Family");
    }

    Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new Human[]{};
        mother.setFamily(this);
        father.setFamily(this);
    }

    Family(Human mother, Human father, Human[] children) {
        this(mother, father);
        this.children = children;

        mother.setFamily(this);
        father.setFamily(this);

        for (Human child : children) {
            child.setFamily(this);
        }
    }


    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Family{")
                .append("mother=" + mother)
                .append(", father=" + father)
                .append(", children=" + Arrays.deepToString(children))
                .append("}");
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father);
        result = 31 * result;
        return result;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.printf("Объект %s удален сборщиком мусора.\n", this.toString());
        super.finalize();

    }

    public Human[] overwriteArray(int index, Human[] children){
        for (int i = index; i < children.length - 1; i++){
            children[i] = children[i+1];
        }
        return Arrays.copyOf(this.children, this.children.length - 1);
    }

    public Human[] addChild(Human child) {
        Human[] newArr = Arrays.copyOf(this.children, this.children.length + 1);
        newArr[newArr.length - 1] = child;
        child.setFamily(this);
        setChildren(newArr); ;
        return this.children;
    }

    public boolean deleteChild(int index){
        if (index > 0 && index < this.children.length){
            this.children[index].setFamily(null);
            Human[] newArr = overwriteArray(index, this.children);
            setChildren(newArr);
            return true;
        }

        return false;
    }

    public Human[] deleteChild(Human child){
        for (int i = 0; i < this.children.length; i++) {
            if (child.equals(this.children[i])) {
                this.children[i].setFamily(null);
                Human[] newArr = overwriteArray(i, this.children);
                setChildren(newArr);
            }
        }
        return this.children;
    }

    public int countFamily(){
        return this.children.length + 2;
    }

    public int getRandomInt(int from, int to){
        Random random = new Random();
        return from + random.nextInt(to - from + 1);
    }

    @Override
    public Human bornChild(){
        int sex = getRandomInt(0,1);
        Human child = (sex == 0) ? new Man() : new Woman();
        child.setFamily(this);
        addChild(child);
        String[] girlNames = new String[] {"АННА", "ГАЛЯ", "ЮЛЯ", "АЛЛА", "ЕВГЕНИЯ", "ОЛЬГА", "ЯНА", "АЛЕНА", "ИННА","МАРИНА"};
        String[] boyNames = new String[] {"ИЛЬЯ", "АНДРЕЙ", "ЮРИЙ", "ОЛЕГ", "ЕВГЕНИЙ", "СЕРГЕЙ", "АЛЕКСАНДР", "АНТОНЯ", "ЯРОСЛАВ","МИХАИЛ"};
        child.setSurname(this.father.getSurname());
        child.setName(sex == 0 ? boyNames[getRandomInt(0,9)] : girlNames[getRandomInt(0,9)]);
        child.setIq(Math.round((mother.getIq() + father.getIq())/2));
        child.setYear(0);
        return child;
    }
}
