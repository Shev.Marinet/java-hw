package homework7.pet;

import homework7.Species;

public class RoboCat extends Pet {

    {
        RoboCat.this.setSpecies(Species.ROBO_CAT);
    }

    public RoboCat(){
        super();
    }

    public RoboCat(String nickname){
        super(nickname);
    }

    public RoboCat(String nickname, int age, int trickLevel, String[] habits){
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond(){
        System.out.printf("Привет, хозяин. Я - %s. Я ждал тебя 8 часов! \n" , this.getNickname());
    }

}
