package homework7.pet;

import homework7.Species;

public class Fish extends Pet {

    {
        Fish.this.setSpecies(Species.FISH);
    }

    public Fish(){
        super();
    }

    public Fish(String nickname){
        super(nickname);
    }

    public Fish(String nickname, int age, int trickLevel, String[] habits){
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond(){
        System.out.printf("Привет, хозяин. Я - %s. Я тихо ждала тебя! \n" , this.getNickname());
    }

}
