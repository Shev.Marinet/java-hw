package homework7.pet;

import homework7.Species;

import java.util.Arrays;
import java.util.Objects;

abstract public class Pet {

    private Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    static {
        System.out.println("Загружается класс Pet");
    }

    {
        System.out.println("Создается объект Pet");
    }

    Pet(){
        nickname = "Шарик";
        age = 5;
        trickLevel = 70;
        habits = new String[]{"спит", "ест", "играет"};
    }

    Pet(String nickname) {
        this.nickname = nickname;
    }

    Pet(String nickname, int age, int trickLevel, String[] habits) {
        this(nickname);

        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public void eat() {
        System.out.println("Я кушаю!");
    }

    public abstract void respond();

    interface Foul{
        void foul();
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public Species getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public boolean isTrick (){
        return this.trickLevel > 50;
    }

    public String printTrick() {
        return this.isTrick() ? "очень хитрый" : "почти не хитрый";
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.species + " {")
                .append("nickname = " + this.nickname)
                .append(this.age != 0 ? ", age = " + this.age : "")
                .append(this.trickLevel != 0 ? ", trickLevel = " +this.trickLevel : "")
                .append(this.habits != null ? ", habits = " + Arrays.deepToString(this.habits) : "")
                .append(getSpecies().canFly() ? ", умеет летать" : ", не умеет летать")
                .append(getSpecies().hasFur() ? ", есть шерсть" : ", нет шерсти")
                .append(", имеет " + getSpecies().numberOfLegs() + " лапы")
                .append("}");

        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;

        return age == pet.age &&
                trickLevel == pet.trickLevel &&
                Objects.equals(nickname, pet.nickname) &&
                Arrays.equals(habits, pet.habits);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(species, nickname, age, trickLevel);
        result = 31 * result + Arrays.hashCode(habits);
        return result;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.printf("Объект %s удален сборщиком мусора.\n", this.toString());
        super.finalize();
    }

}


