package homework7.pet;

import homework7.Species;

public class Dog extends Pet implements Pet.Foul {

    {
        Dog.this.setSpecies(Species.DOG);
    }

    public Dog(){
        super();
    }

    public Dog(String nickname){
        super(nickname);
    }

    public Dog(String nickname, int age, int trickLevel, String[] habits){
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond(){
        System.out.printf("Привет, хозяин. Я - %s. Я охранял твой дом! \n" , this.getNickname());
    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы... и спрятаться под столом");
    }

}
