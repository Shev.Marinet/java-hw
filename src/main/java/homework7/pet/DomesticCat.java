package homework7.pet;

import homework7.Species;

public class DomesticCat extends Pet implements Pet.Foul {

    {
        DomesticCat.this.setSpecies(Species.DOMESTIC_CAT);
    }

    public DomesticCat(){
        super();
    }

    public DomesticCat(String nickname){
        super(nickname);
    }

    public DomesticCat(String nickname, int age, int trickLevel, String[] habits){
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond(){
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился! \n" , this.getNickname());
    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы... и залезть под кровать");
    }

}
