package homework8;

import homework8.human.Human;
import homework8.human.Man;
import homework8.human.Woman;
import homework8.pet.Dog;
import homework8.pet.DomesticCat;
import homework8.pet.Pet;
import homework8.pet.RoboCat;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

        Map<String, String> schedule = new HashMap<>() ;
        schedule.put(DayOfWeek.TUESDAY.name(), "футбол");
        schedule.put(DayOfWeek.THURSDAY.name(), "бассейн");
        schedule.put(DayOfWeek.SATURDAY.name(), "робототехника");

        Dog sharik = new Dog();
        DomesticCat murka = new DomesticCat("Мурка");
        Pet ryzhyk = new RoboCat("Рыжик", 7, 20, new HashSet<>(Arrays.asList("прыгает", "играет")));

        Man yuriy = new Man("Юрий", "Иванов", 35,64);
        Woman olga = new Woman("Ольга", "Иванова", 32,75);
        Human yana = new Woman("Яна", "Иванова", 9, new HashSet<>(Arrays.asList(murka)));

        Family ivanovy = new Family(olga,yuriy);
        ivanovy.addChild(yana);

        Human ivan = new Man();
        Human ann = new Woman("Анна", "Попова", 30);
        Human andrew = new Man("Андрей", "Попов", 12, 60, schedule,new HashSet<>(Arrays.asList(ryzhyk)));
        Family popovy = new Family(ann, ivan);

        popovy.addChild(andrew);
        popovy.addChild(yana);
        popovy.deleteChild(1);

        System.out.println(sharik);
        System.out.println(murka.getSpecies().canFly());
        System.out.println(murka.getSpecies().hasFur());
        System.out.println(murka.getSpecies().numberOfLegs());
        System.out.println(ryzhyk);

        System.out.println(yuriy);
        System.out.println(olga);
        System.out.println(yana);
        System.out.println(ivanovy);

        System.out.println(ivan);
        System.out.println(ann);
        System.out.println(andrew);
        System.out.println(popovy);

        System.out.println(andrew.getFamily());
        System.out.println(yana.getFamily());
        System.out.println(popovy.countFamily());

        popovy.addChild(yana);
        System.out.println(popovy);
        popovy.deleteChild(andrew);
        System.out.println(popovy);
        System.out.println(andrew.getFamily());

        yana.greetPets();
        yana.describePet();

        andrew.greetPets();
        andrew.describePet();

        yana.getPets().stream().findFirst().get().eat();
        yana.getPets().stream().findFirst().get().respond();
        murka.foul();

        yana.isTimeToFeed(true, murka);
        yana.isTimeToFeed(false, murka);

        yuriy.repairCar();
        olga.makeup();

        System.out.println(ivanovy.bornChild());
    }
}
