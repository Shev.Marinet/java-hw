package homework8.human;

import homework8.pet.Pet;

import java.util.Map;
import java.util.Set;

public final class Man extends Human {

    public Man(){
        super();
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, Set<Pet> pets) {
        super(name, surname, year, pets);
    }

    public Man(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
    }

    public Man(String name, String surname, int year, int iq, Map schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Man(String name, String surname, int year, int iq, Map schedule, Set<Pet> pets) {
        super(name, surname, year, iq, schedule, pets);
    }

    @Override
    public void greetPets() {
        if (isPresentPet()) {
            String petsName = "";
            int counter = 0;
            for (Pet pet : getPets()) {
                petsName += pet.getNickname();
                counter++;
                if (counter < getPets().size()) petsName += ", ";
            }
            System.out.printf("Привет, %s. Где мои тапки?\n", petsName);
        }
    }

    public void repairCar() {
        System.out.println("Пора сальники поменять");
    }
}
