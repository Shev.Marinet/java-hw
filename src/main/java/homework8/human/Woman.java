package homework8.human;

import homework8.pet.Pet;

import java.util.Map;
import java.util.Set;

public final class Woman extends Human {

    public Woman(){
        this.setName("Инна");
        this.setSurname("Попова");
        this.setYear(32);
        this.setIq(70);
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, Set<Pet> pets) {
        super(name, surname, year, pets);
    }

    public Woman(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
    }

    public Woman(String name, String surname, int year, int iq, Map schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Woman(String name, String surname, int year, int iq, Map schedule, Set<Pet> pets) {
        super(name, surname, year, iq, schedule, pets);
    }

    @Override
    public void greetPets() {
        if (isPresentPet()) {
            String petsName = "";
            int counter = 0;
            for (Pet pet : getPets()) {
                petsName += pet.getNickname();
                counter++;
                if (counter < getPets().size()) petsName += ", ";
            }
            System.out.printf("Привет, %s. Я по тебе соскучилась!\n", petsName);
        }
    }

    public void makeup() {
        System.out.println("Пора сделать вечерий макияж");
    }
}
