package homework8.human;

import homework8.Family;
import homework8.pet.Pet;

import java.util.*;

abstract public class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
    private Set<Pet> pets;
    private Family family;
    private Map schedule;

    static {
        System.out.println("Загружается класс Human");
    }

    {
        System.out.println("Создается объект Human");
    }

    Human(){
        name = "Ваня";
        surname = "Попов";
        year = 32;
        iq = 70;
    }

    Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    Human(String name, String surname, int year, Set<Pet> pets) {
        this(name, surname, year);
        this.pets = pets;
    }

    Human(String name, String surname, int year, int iq) {
        this(name, surname, year);
        this.iq = iq;
    }

    Human(String name, String surname, int year, int iq, Map schedule) {
        this(name, surname, year);
        this.iq = iq;
        this.schedule = schedule;
    }

    Human(String name, String surname, int year, int iq, Map schedule, Set<Pet> pets) {
        this(name, surname, year, iq, schedule);
        this.pets = pets;
    }

    public String printMap(Map schedule) {
        String result = "[";
        int counter = 0;
        for (Object name : schedule.keySet()) {
            String key = name.toString();
            String value = schedule.get(name).toString();
            result = result + "[" + key + ", " + value + "]";
            counter++;
            if (counter < schedule.size()) result += ", ";
        }
        return result + "]";
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Human {")
                .append("name = " + this.name)
                .append(", surname = " + this.surname)
                .append(", year = " +this.year)
                .append(this.iq != 0 ? ", iq = " + this.iq : "")
                .append(this.schedule != null ? ", schedule = " + printMap(schedule) : "")
                .append("}");
        return builder.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.printf("Объект %s удален сборщиком мусора.\n", this.toString());
        super.finalize();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Map getSchedule() {
        return schedule;
    }

    public void setSchedule(Map schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Set<Pet> getPets() {
        return pets;
    }

    public void setPets(Set<Pet> pets) {
        this.pets = pets;
    }

    public abstract void greetPets();

    public void describePet() {
        if (isPresentPet()) {
            String result = "У меня есть ";
            for (Pet pet : pets){
                result += pet.getSpecies() + ", ему " + pet.getAge() + " лет, он " + pet.getTrickLevel();
            }
            System.out.println(result);
        }
    }

    public boolean isTimeToFeed(boolean isTime, Pet pet) {
        if (isPresentPet() && this.pets.contains(pet)) {
            int random = getRandomInt(0, 100);
            if (isTime || pet.getTrickLevel() > random) {
                System.out.println("Хм... покормлю-ка я " + pet.getNickname() + "a.");
                return true;
            }

            System.out.printf("Думаю, %s не голоден. \n", pet.getNickname());
            return false;

        }
        return false;
    }

    int getRandomInt(int from, int to) {
        Random random = new Random();
        return from + random.nextInt(to - from + 1);
    }

    boolean isPresentPet() {
        return this.pets != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year &&
                iq == human.iq &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(schedule, human.schedule);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, surname, year, iq);
        result = 31 * result + Objects.hashCode(schedule);
        return result;
    }

}
