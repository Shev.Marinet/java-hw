package homework11.controller;

import homework11.Family;
import homework11.human.Human;
import homework11.pet.Pet;
import homework11.service.FamilyService;

import java.util.List;
import java.util.Set;

public class FamilyController {

    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies(){
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int number) {
        return familyService.getFamiliesBiggerThan(number);
    }

    public List<Family> getFamiliesLessThan(int number) {
        return familyService.getFamiliesLessThan(number);
    }

    public int countFamiliesWithMemberNumber(int number) {
        return familyService.countFamiliesWithMemberNumber(number);
    }

    public Family createNewFamily(Human mother, Human father) {
        return familyService.createNewFamily(mother, father);
    }

    public Family deleteFamilyByIndex(int index) {
        return familyService.deleteFamilyByIndex(index);
    }

    public Human bornChild(Family family, String girlName, String boyName) {
        return familyService.bornChild(family, girlName, boyName);
    }

    public boolean adoptChild(Family family, Human child) {
        return familyService.adoptChild(family, child);
    }

    public boolean deleteAllChildrenOlderThen (int age) {
        return familyService.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    public Set<Pet> getPets(int index) {
        return familyService.getPets(index);
    }

    public Family addPet(int index, Pet pet) {
        return familyService.addPet(index, pet);
    }

}
