package homework11.service;

import homework11.Family;
import homework11.dao.FamilyDao;
import homework11.human.Human;
import homework11.pet.Pet;

import java.util.*;

import java.util.stream.Collectors;

public class FamilyService {

    private FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyDao.getAllFamilies().stream().forEach(family -> System.out.print("[" + family + "] "));
    }

    public List<Family> getFamiliesBiggerThan(int number) {
        return getAllFamilies()
                .stream()
                .filter(f -> f.countFamily() > number)
                .collect(Collectors.toList());
    }

    public List<Family> getFamiliesLessThan(int number) {
        return getAllFamilies()
                .stream()
                .filter(f -> f.countFamily() < number)
                .collect(Collectors.toList());

    }

    public int countFamiliesWithMemberNumber(int number) {
        return (int) getAllFamilies().stream().filter(f -> f.countFamily() == number).count();
    }

    public Family createNewFamily(Human mother, Human father) {
        Family family = new Family(mother, father);
        familyDao.saveFamily(family);
        return family;
    }

    public Family deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public Human bornChild(Family family, String girlName, String boyName) {
        return family.bornChild(girlName, boyName);
    }

    public boolean adoptChild(Family family, Human child) {
        return family.addChild(child) != null;
    }

    public boolean deleteAllChildrenOlderThen(int age) {
        List<Family> famInitial = getAllFamilies();
        List<Family> fam = familyDao.getAllFamilies()
                .stream()
                .peek(f -> f.getChildren().removeIf(child -> child.getAge() >= age))
                .collect(Collectors.toList());
        return !fam.equals(famInitial);
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return familyDao.getAllFamilies().get(index);
    }

    public Set<Pet> getPets(int index) {
        return getAllFamilies().get(index).getPets();
    }

    public Family addPet(int index, Pet pet) {
        Family family = familyDao.getFamilyByIndex(index);
        family.setPets(new HashSet<>(Arrays.asList(pet)));
        return family;
    }

}
