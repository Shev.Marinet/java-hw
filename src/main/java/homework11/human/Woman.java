package homework11.human;

import homework11.pet.Pet;

import java.text.ParseException;
import java.util.Map;

public final class Woman extends Human {

    public Woman(){
        this.setName("Инна");
        this.setSurname("Попова");
        this.setBirthDate(32);
        this.setIq(70);
    }

    public Woman(String name, String surname, long birthDate) {
        super(name, surname, birthDate);
    }

    public Woman(String name, String surname, long birthDate, int iq) {
        super(name, surname, birthDate, iq);
    }


    public Woman(String name, String surname, String birthDate, int iq) throws ParseException {
        super(name, surname, birthDate, iq);
    }

    public Woman(String name, String surname, long birthDate, int iq, Map schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    @Override
    public void greetPets() {
        if (isPresentPet()) {
            String petsName = "";
            int counter = 0;
            for (Pet pet : getFamily().getPets()) {
                petsName += pet.getNickname();
                counter++;
                if (counter < getFamily().getPets().size()) petsName += ", ";
            }
            System.out.printf("Привет, %s. Я по тебе соскучилась!\n", petsName);
        }
    }

    public void makeup() {
        System.out.println("Пора сделать вечерий макияж");
    }
}
