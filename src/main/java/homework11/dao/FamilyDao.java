package homework11.dao;

import homework11.Family;

import java.util.List;

public interface FamilyDao {

    List<Family> getAllFamilies();
    Family getFamilyByIndex(int index);
    Family deleteFamily(int index);
    boolean deleteFamily(Family family);
    List<Family> saveFamily(Family family);

}
