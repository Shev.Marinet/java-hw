package homework11.pet;

import homework11.Species;

import java.util.Set;

public class RoboCat extends Pet {

    {
        RoboCat.this.setSpecies(Species.ROBO_CAT);
    }

    public RoboCat(){
        super();
    }

    public RoboCat(String nickname){
        super(nickname);
    }

    public RoboCat(String nickname, int age, int trickLevel){
        super(nickname, age, trickLevel);
    }

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits){
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond(){
        System.out.printf("Привет, хозяин. Я - %s. Я ждал тебя 8 часов! \n" , this.getNickname());
    }

}
