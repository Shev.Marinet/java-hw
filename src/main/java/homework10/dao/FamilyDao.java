package homework10.dao;

import homework10.Family;

import java.util.List;

public interface FamilyDao {

    List<Family> getAllFamilies();
    Family getFamilyByIndex(int index);
    Family deleteFamily(int index);
    boolean deleteFamily(Family family);
    List<Family> saveFamily(Family family);

}
