package homework10.human;

public interface HumanCreator {
    public Human bornChild(String girlName, String boyName);
}
