package homework8.pet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class PetTest {

    private  Pet pet = new DomesticCat("Lucky", 6, 40, new HashSet<>(Arrays.asList("прыгает", "играет")));
    private  Pet pet1 = new DomesticCat("Lucky", 6, 40, new HashSet<>(Arrays.asList("прыгает", "играет")));
    private  Pet pet2 = new DomesticCat("Lucky", 6, 40, new HashSet<>(Arrays.asList("прыгает", "играет")));
    private  Pet pet3 = new Dog("Lucky", 6, 40, new HashSet<>(Arrays.asList("прыгает", "играет")));

    @Test
    public void testThatToStingReturnsProperResult() {
        String expectedResult = "DOMESTIC_CAT {nickname = Lucky, age = 6, trickLevel = 40, habits = [прыгает, играет], не умеет летать, есть шерсть, имеет 4 лапы}";
        String actualResult = pet.toString();
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsReturnsTrue() {
        boolean expectedResult = true;
        boolean actualResult = pet.equals(pet2);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsReturnsFalse() {
        boolean expectedResult = false;
        boolean actualResult = pet.equals(pet3);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementReflexiveContract() {
        boolean expectedResult = true;
        boolean actualResult = pet.equals(pet);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementSymmetricContract() {
        boolean expectedResult = true;
        boolean actualResult = pet.equals(pet2) && pet2.equals(pet);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementTransitiveContract() {
        boolean expectedResult = true;
        boolean actualResult = pet.equals(pet1) && pet1.equals(pet2) && pet.equals(pet2);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementConsistentContract() {
        boolean firstResult = pet.equals(pet1);
        boolean secondResult = pet.equals(pet1);
        boolean expectedResult = firstResult && secondResult;
        boolean actualResult = pet.equals(pet1) && pet.equals(pet1);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementComparisionWithZeroContract() {
        boolean expectedResult = false;
        boolean actualResult = pet.equals(null);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatHashCodeReturnsProperResult() {
        int expectedResult = (31 * (Objects.hash(pet.getSpecies(), pet.getNickname(), pet.getAge(), pet.getTrickLevel()))) + Objects.hashCode(pet.getHabits());
        int actualResult = pet.hashCode();
        assertEquals(expectedResult, actualResult);
    }
}
