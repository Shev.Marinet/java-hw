package homework8.human;

import homework8.DayOfWeek;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class HumanTest {

    Map<String, String> schedule = new HashMap<>() ;

    @Before
    public void initSchedule() {
        schedule.put(DayOfWeek.TUESDAY.name(), "футбол");
        schedule.put(DayOfWeek.THURSDAY.name(), "бассейн");
        schedule.put(DayOfWeek.SATURDAY.name(), "робототехника");
    }

    private Human human = new Man("Андрей", "Попов", 12, 60);
    private Human human1 = new Man("Андрей", "Попов", 12, 60);
    private Human human2 = new Man("Андрей", "Попов", 12, 60);
    private Human human3 = new Man("Олег", "Попов", 12, 60);


    @Test
    public void testThatToStingReturnsProperResult() {
        String expectedResult = "Human {name = Андрей, surname = Попов, year = 12, iq = 60}";
        String actualResult = human.toString();
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsReturnsTrue() {
        boolean expectedResult = true;
        boolean actualResult = human.equals(human2);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsReturnsFalse() {
        boolean expectedResult = false;
        boolean actualResult = human.equals(human3);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementReflexiveContract() {
        boolean expectedResult = true;
        boolean actualResult = human.equals(human);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementSymmetricContract() {
        boolean expectedResult = true;
        boolean actualResult = human.equals(human2) && human2.equals(human);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementTransitiveContract() {
        boolean expectedResult = true;
        boolean actualResult = human.equals(human1) && human1.equals(human2) && human.equals(human2);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementConsistentContract() {
        boolean firstResult = human.equals(human1);
        boolean secondResult = human.equals(human1);
        boolean expectedResult = firstResult && secondResult;
        boolean actualResult = human.equals(human1) && human.equals(human1);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementComparisionWithZeroContract() {
        boolean expectedResult = false;
        boolean actualResult = human.equals(null);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatHashCodeReturnsProperResult() {
        int expectedResult = (31 * (Objects.hash(human.getName(), human.getSurname(), human.getYear(), human.getIq()))) + Objects.hashCode(human.getSchedule());
        int actualResult = human.hashCode();
        assertEquals(expectedResult, actualResult);
    }
}

