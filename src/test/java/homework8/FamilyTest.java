package homework8;

import homework8.human.Human;
import homework8.human.Man;
import homework8.human.Woman;
import homework8.pet.Pet;
import homework8.pet.RoboCat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.*;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class FamilyTest {

    private Man ivan = new Man();
    private Woman ann = new Woman("Анна", "Попова", 30);
    private Human inna = new Woman("Инна", "Лещенко", 32);

    private Human yana = new Woman("Яна", "Иванова", 9);
    private Human andrew = new Man("Андрей", "Попов", 12);
    private Human olga = new Woman("Ольга", "Круглова", 10);

    List<Human> children = new ArrayList<>(Arrays.asList(yana, andrew));
    List<Human> children2 = new ArrayList<>(Arrays.asList(yana, andrew, olga));

    private Pet ryzhyk = new RoboCat("Рыжик", 7, 20, new HashSet<>(Arrays.asList("прыгает", "играет")));

    private Family family = new Family(ann, ivan);
    private Family family1 = new Family(ann, ivan);
    private Family family2 = new Family(ann, ivan);
    private Family family3 = new Family(inna, ivan);
    private Family family4 = new Family(ann, ivan, children);


    @Test
    public void testThatToStingReturnsProperResult() {
        String expectedResult = "Family{mother=Human {name = Анна, surname = Попова, year = 30}, father=Human {name = Ваня, surname = Попов, year = 32, iq = 70}, children=[]}";
        String actualResult = family.toString();
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatDeleteChildDeleteItIfPresent() {
        String expectedResult = Arrays.toString(new Human[]{andrew});
        String actualResult = family4.deleteChild(yana) ? Arrays.toString(family4.getChildren().toArray()) : "";
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatDeleteChildDontDeleteItIfAbsent() {
        String expectedResult = Arrays.toString(children.toArray());
        String actualResult = Arrays.toString(family4.getChildren().toArray());
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatDeleteChildByIndexDeleteItIfPresent() {
        boolean expectedResult = true;
        boolean actualResult = family4.deleteChild(1);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatDeleteChildByIndexDontDeleteItIfAbsent() {
        boolean expectedResult = false;
        boolean actualResult = family4.deleteChild(3);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatAddChildReturnsProperResult() {
        String expectedResult = Arrays.toString(children2.toArray());
        String actualResult = Arrays.toString(family4.addChild(olga).toArray());
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatCountFamilyReturnsProperResult() {
        int expectedResult = 4;
        int actualResult = family4.countFamily();
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsReturnsTrue() {
        boolean expectedResult = true;
        boolean actualResult = family.equals(family2);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsReturnsFalse() {
        boolean expectedResult = false;
        boolean actualResult = family.equals(family3);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementReflexiveContract() {
        boolean expectedResult = true;
        boolean actualResult = family.equals(family);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementSymmetricContract() {
        boolean expectedResult = true;
        boolean actualResult = family.equals(family2) && family2.equals(family);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementTransitiveContract() {
        boolean expectedResult = true;
        boolean actualResult = family.equals(family1) && family1.equals(family2) && family.equals(family2);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementConsistentContract() {
        boolean firstResult = family.equals(family1);
        boolean secondResult = family.equals(family1);
        boolean expectedResult = firstResult && secondResult;
        boolean actualResult = family.equals(family1) && family.equals(family1);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementComparisionWithZeroContract() {
        boolean expectedResult = false;
        boolean actualResult = family.equals(null);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatHashCodeReturnsProperResult() {
        int expectedResult = 31 * (Objects.hash(family.getMother(), family.getFather()));
        int actualResult = family.hashCode();
        assertEquals(expectedResult, actualResult);
    }

}

