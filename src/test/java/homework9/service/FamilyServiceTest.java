package homework9.service;

import homework9.Family;
import homework9.dao.CollectionFamilyDao;
import homework9.dao.FamilyDao;
import homework9.human.Human;
import homework9.human.Man;
import homework9.human.Woman;
import homework9.pet.Pet;
import homework9.pet.RoboCat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.*;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class FamilyServiceTest {

    FamilyDao fd = new CollectionFamilyDao();
    FamilyService fs = new FamilyService(fd);

    Man yuriy = new Man("Yuriy", "Ivanov", 35,64);
    Woman olga = new Woman("Olga", "Ivanova", 32,75);

    Man ivan = new Man("Ivan", "Popov", 33,90);
    Woman ann = new Woman("Ann", "Popova", 30,78);

    Man oleg = new Man("Oleg", "Kosenko", 35,64);
    Woman inna = new Woman("Inna", "Kosenko", 32,75);

    Man vlad = new Man("Vlad", "Panin", 33,90);
    Woman yana = new Woman("Yana", "Panina", 30,78);

    Pet ryzhyk = new RoboCat("Рыжик", 7, 60);
    Man andrew = new Man("Andrew", "Popov", 12);

    Family ivanovy = new Family(olga,yuriy);
    Family popovy = new Family(ann,ivan);
    Family kosenko = new Family(inna, oleg);
    Family paniny = new Family(yana, vlad);

    @Before
    public void initFamilies(){
        Family ivanovy = fs.createNewFamily(olga,yuriy);
        Family popovy = fs.createNewFamily(ann,ivan);
        Family kosenko = fs.createNewFamily(inna, oleg);
        ivanovy.bornChild("Sveta", "Oleg");
        ivanovy.addChild(andrew);
        popovy.setPets(new HashSet<>(Arrays.asList(ryzhyk)));

    }

    @Test
    public void testThatGetAllFamiliesReturnsProperResult(){
        List expectedResult = new ArrayList(Arrays.asList(ivanovy, popovy,kosenko));
        List actualResult = fd.getAllFamilies();
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatGetFamiliesBiggerThanProperResult() {
        List expectedResult = new ArrayList(Arrays.asList(ivanovy));
        List actualResult = fs.getFamiliesBiggerThan(2);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatGetFamiliesLessThanProperResult() {
        List expectedResult = new ArrayList(Arrays.asList(popovy, kosenko,paniny));
        List actualResult = fs.getFamiliesLessThan(3);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatCountFamiliesWithMemberNumberProperResult() {
        int expectedResult = 0;
        int actualResult = fs.countFamiliesWithMemberNumber(3);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatCreateNewFamilyProperResult() {
        Family expectedResult = paniny;
        Family actualResult = fs.createNewFamily(yana,vlad);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatDeleteFamilyByIndexProperResult() {
        Family expectedResult = ivanovy;
        Family actualResult = fs.deleteFamilyByIndex(0);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatBornChildProperResult() {
        Human firstExpectedResult = new Man("Maks", "Popov", 0,84);
        Human secondExpectedResult = new Woman("Marina", "Popov", 0,84);

        Human actualResult = fs.bornChild(popovy,"Marina", "Maks");

        boolean firstExpectedResultBool = firstExpectedResult.equals(actualResult);

        Human expectedResult = secondExpectedResult;
        if (firstExpectedResultBool){
            expectedResult = firstExpectedResult;
        }

        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatAdoptChildProperResult() {
        boolean expectedResult = true;
        boolean actualResult = fs.adoptChild(popovy, andrew);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatDeleteAllChildrenOlderThenProperResult () {
        boolean expectedResult = true;
        boolean actualResult = fs.deleteAllChildrenOlderThen(5);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatCountProperResult() {
        int expectedResult = 3;
        int actualResult = fs.count();
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatGetFamilyByIdProperResult() {
        Family expectedResult = popovy;
        Family actualResult = fs.getFamilyById(0);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatGetPetsProperResult() {
        Set expectedResult = new HashSet(Arrays.asList(ryzhyk));
        Set actualResult = fs.getPets(0);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatAddPetProperResult() {
        Family expectedResult = ivanovy;
        Family actualResult = fs.addPet(2, ryzhyk);
        assertEquals(expectedResult, actualResult);
    }
}
