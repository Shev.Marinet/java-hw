package homework6;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.Objects;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class FamilyTest {

    private Human Ivan = new Human();
    private Human Ann = new Human("Анна", "Попова", 30);
    private Human Inna = new Human("Инна", "Лещенко", 32);

    private Human Yana = new Human("Яна", "Иванова", 9);
    private Human Andrew = new Human("Андрей", "Попов", 12);
    private Human Olga = new Human("Ольга", "Круглова", 10);

    Human[] children = new Human[]{Yana, Andrew};
    Human[] children2 = new Human[]{Yana, Andrew,Olga};

    private Pet Ryzhyk = new Pet(Species.СОБАКА, "Рыжик", 7, 20, new String[]{"прыгает", "играет"});

    private Family family = new Family(Ann, Ivan, Ryzhyk);
    private Family family1 = new Family(Ann, Ivan, Ryzhyk);
    private Family family2 = new Family(Ann, Ivan, Ryzhyk);
    private Family family3 = new Family(Inna, Ivan, Ryzhyk);
    private Family family4 = new Family(Ann, Ivan, children, Ryzhyk);


    @Test
    public void testThatToStingReturnsProperResult() {
        String expectedResult = "Family{mother=Human {name = Анна, surname = Попова, year = 30}, father=Human {name = Ваня, surname = Попов, year = 32, iq = 70}, children=[], pet=СОБАКА {nickname = Рыжик, age = 7, trickLevel = 20, habits = [прыгает, играет], не умеет летать, есть шерсть, имеет 4 лапы}";
        String actualResult = family.toString();
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatDeleteChildDeleteItIfPresent() {
        String expectedResult = Arrays.toString(new Human[]{Andrew});
        String actualResult = Arrays.toString(family4.deleteChild(Yana));
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatDeleteChildDontDeleteItIfAbsent() {
        String expectedResult = Arrays.toString(children);
        String actualResult = Arrays.toString(family4.deleteChild(Olga));
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatDeleteChildByIndexDeleteItIfPresent() {
        boolean expectedResult = true;
        boolean actualResult = family4.deleteChild(1);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatDeleteChildByIndexDontDeleteItIfAbsent() {
        boolean expectedResult = false;
        boolean actualResult = family4.deleteChild(3);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatAddChildReturnsProperResult() {
        String expectedResult = Arrays.toString(children2);
        String actualResult = Arrays.toString(family4.addChild(Olga));
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatCountFamilyReturnsProperResult() {
        int expectedResult = 4;
        int actualResult = family4.countFamily();
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsReturnsTrue() {
        boolean expectedResult = true;
        boolean actualResult = family.equals(family2);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsReturnsFalse() {
        boolean expectedResult = false;
        boolean actualResult = family.equals(family3);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementReflexiveContract() {
        boolean expectedResult = true;
        boolean actualResult = family.equals(family);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementSymmetricContract() {
        boolean expectedResult = true;
        boolean actualResult = family.equals(family2) && family2.equals(family);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementTransitiveContract() {
        boolean expectedResult = true;
        boolean actualResult = family.equals(family1) && family1.equals(family2) && family.equals(family2);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementConsistentContract() {
        boolean firstResult = family.equals(family1);
        boolean secondResult = family.equals(family1);
        boolean expectedResult = firstResult && secondResult;
        boolean actualResult = family.equals(family1) && family.equals(family1);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementComparisionWithZeroContract() {
        boolean expectedResult = false;
        boolean actualResult = family.equals(null);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatHashCodeReturnsProperResult() {
        int expectedResult = 31 * (Objects.hash(family.getMother(), family.getFather()));
        int actualResult = family.hashCode();
        assertEquals(expectedResult, actualResult);
    }

}

