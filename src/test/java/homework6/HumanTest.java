package homework6;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.Objects;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class HumanTest {

    String[][] schedule = new String[3][2];

    @Before
    public void initSchedule() {
        schedule[0][0] = DayOfWeek.ВТОРНИК.name();
        schedule[0][1] = "футбол";
        schedule[1][0] = DayOfWeek.ЧЕТВЕРГ.name();
        schedule[1][1] = "бассейн";
        schedule[2][0] = DayOfWeek.СУББОТА.name();
        schedule[2][1] = "робототехника";
    }

    private Human human = new Human("Андрей", "Попов", 12, 60,schedule);
    private Human human1 = new Human("Андрей", "Попов", 12, 60,schedule);
    private Human human2 = new Human("Андрей", "Попов", 12, 60,schedule);
    private Human human3 = new Human("Олег", "Попов", 12, 60,schedule);


    @Test
    public void testThatToStingReturnsProperResult() {
        String expectedResult = "Human {name = Андрей, surname = Попов, year = 12, iq = 60, schedule = [[ВТОРНИК, футбол], [ЧЕТВЕРГ, бассейн], [СУББОТА, робототехника]]}";
        String actualResult = human.toString();
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsReturnsTrue() {
        boolean expectedResult = true;
        boolean actualResult = human.equals(human2);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsReturnsFalse() {
        boolean expectedResult = false;
        boolean actualResult = human.equals(human3);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementReflexiveContract() {
        boolean expectedResult = true;
        boolean actualResult = human.equals(human);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementSymmetricContract() {
        boolean expectedResult = true;
        boolean actualResult = human.equals(human2) && human2.equals(human);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementTransitiveContract() {
        boolean expectedResult = true;
        boolean actualResult = human.equals(human1) && human1.equals(human2) && human.equals(human2);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementConsistentContract() {
        boolean firstResult = human.equals(human1);
        boolean secondResult = human.equals(human1);
        boolean expectedResult = firstResult && secondResult;
        boolean actualResult = human.equals(human1) && human.equals(human1);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatEqualsImplementComparisionWithZeroContract() {
        boolean expectedResult = false;
        boolean actualResult = human.equals(null);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatHashCodeReturnsProperResult() {
        int expectedResult = (31 * (Objects.hash(human.getName(), human.getSurname(), human.getYear(), human.getIq()))) + Arrays.hashCode(human.getSchedule());
        int actualResult = human.hashCode();
        assertEquals(expectedResult, actualResult);
    }
}

